﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour {


    public float damage;
    public float speed;

    //When the bullet is made, this starts the timer that destroys the bullet in case it misses
    void Start()
    {
        StartCoroutine(BulletTimeOut());
    }

    // Update is called once per frame, looks at the enemy and moves the bullet toward it
    void Update()
    {
        transform.position += transform.forward * Time.deltaTime * speed;
    }

    //If the bullet hits the player it takes damage (the damage is currently equal to bulletPower located in EnemyStats)
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {           
            other.GetComponent<Stats>().health -= damage;
            Destroy(gameObject);
        }
    }

    //Destroy's the bullet after 2 seconds
    IEnumerator BulletTimeOut()
    {
        yield return new WaitForSeconds(12);
        Destroy(gameObject);
    }
}
