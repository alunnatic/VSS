﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour {

    public Transform enemy1;
    public float health;
    public float armor;
    public float enemySpeed;
    public float fireRate;
    public float bulletSpeed;
    public float bulletPower;


    
    // Use this for initialization
    void Start()
    {

    }
    
    // Update is called once per frame
    void Update()
    {
        if(health < 1)
        {
            Destroy(gameObject);
        }
    }

    public void setAll(float h, float a, float es, float fr, float bs, float bp)
    {
        health = h;
        armor = a;
        enemySpeed = es;
        fireRate = fr;
        bulletSpeed = bs;
        bulletPower = bp;
        GetComponent<EnemyShoot>().setFireDelta(fr);
    }

}
