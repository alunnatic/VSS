﻿using MoveTypes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public class MoveEnemyNew : MonoBehaviour
{
    // I'm using a List<MoveTypeBase> which means it can take any object which inherits from MoveTypeBase
    // this lets us have a list with Move objects and Turn objects, etc.,
    // which share a parent object and can share properties,
    // but at the same time can have their own properties and implementations.
    // That way we don't end up with one big object that has properties that are only relevent to certain move types
    public List<MoveTypeBase> moveList = new List<MoveTypeBase>();

    /// <summary>
    /// Starts doing the moves in moveList
    /// Since we have to put the moves in moveList (from another script or some other way)
    /// I avoided putting this in Start()
    /// </summary>
    public void StartMoves()
    {
        // couroutines need to be started with the StartCoroutine call for them to work correctly
        StartCoroutine(DoMoves());
    }

    /// <summary>
    /// Does each of the moves in moveList,
    /// waiting for each one to finish before going to the next.
    /// It does this by using Coroutines, which lets us have a function that can run over multiple frames
    /// </summary>
    IEnumerator DoMoves()
    {
        foreach (var move in moveList)
        {
            // yield return StartCoroutine(___) tells it to wait until the coroutine finishes before moving on
            yield return StartCoroutine(move.DoMove());
        }
    }
}
