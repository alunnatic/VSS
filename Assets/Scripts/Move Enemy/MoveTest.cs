﻿using MoveTypes;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MoveEnemyNew))]
public class MoveTest : MonoBehaviour {
    MoveEnemyNew moveEnemy;
    public Transform enemy1;
    //arguments are direction (vector3), speed, distance
    //right is Vector3.back
    //left is Vector3.forward
    //up is Vector3.right
    //down is Vector3.left

	void Start () {
        /*
        moveEnemy = GetComponent<MoveEnemyNew>();
        moveEnemy.moveList = new List<MoveTypeBase>()
        {
            
            new MoveTypes.Turn(transform, Quaternion.Euler(0, 180, 0), 1 * Mathf.PI),
            new MoveTypes.Turn(transform, Quaternion.Euler(0, 180, 0), 1 * Mathf.PI),
            new MoveTypes.Turn(transform, Quaternion.Euler(0, 180, 0), 1 * Mathf.PI),
            new MoveTypes.Turn(transform, Quaternion.Euler(0, 180, 0), 1 * Mathf.PI),
            new MoveTypes.Turn(transform, Quaternion.Euler(0, -180, 0), 1 * Mathf.PI),
            new MoveTypes.Turn(transform, Quaternion.Euler(0, -180, 0), 1 * Mathf.PI),
            new MoveTypes.Turn(transform, Quaternion.Euler(0, -180, 0), 1 * Mathf.PI),
            new MoveTypes.Turn(transform, Quaternion.Euler(0, -180, 0), 1 * Mathf.PI),
            
            new MoveTypes.Turn(transform, Quaternion.Euler(0, 180, 0), 2 * Mathf.PI),
            new MoveTypes.Move(transform, Vector3.back, 3, 2),
            new MoveTypes.Turn(transform, Quaternion.Euler(0, 180, 0), 2 * Mathf.PI),
            new MoveTypes.Move(transform, Vector3.forward, 3, 2),
            new MoveTypes.Turn(transform, Quaternion.Euler(0, -90, 0), 2 * Mathf.PI),
            new MoveTypes.Move(transform, Vector3.left, 3, 2),
            new MoveTypes.Turn(transform, Quaternion.Euler(0, 180, 0), 2 * Mathf.PI),
            new MoveTypes.Move(transform, Vector3.right, 3, 2),
            //new MoveTypes.Move(transform, new Vector3(1, -1, 0), 3, 5),
            //new MoveTypes.Turn(transform, Quaternion.Euler(0, -45, 0), 2 * Mathf.PI),
            //new MoveTypes.Move(transform, Vector3.down, 3, 2),
            
        };

        moveEnemy.StartMoves();
        */
	}
	
	void Update () {
		
	}

    public void JustDoIt() {
        moveEnemy = GetComponent<MoveEnemyNew>();
        moveEnemy.moveList = new List<MoveTypeBase>()
        {
            new MoveTypes.Turn(transform, Quaternion.Euler(0, -90, 0), 1 * Mathf.PI),
            new MoveTypes.Move(transform, Vector3.left, 1, 16),
            new MoveTypes.Turn(transform, Quaternion.Euler(0, 180, 0), 1 * Mathf.PI),
            new MoveTypes.Move(transform, Vector3.right, 1, 12),
        };
        moveEnemy.StartMoves();
    }
}
