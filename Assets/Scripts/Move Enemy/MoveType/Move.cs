﻿using System;
using System.Collections;
using UnityEngine;

namespace MoveTypes
{
    // <summary>
    /// Basic move action
    /// Moves in a direction at a speed for a distance
    /// </summary>
    public class Move : MoveTypeBase
    {
        Transform transform;
        Vector3 direction;
        float speed;
        float distance;

        public Move(Transform transform, Vector3 direction, float speed, float distance)
        {
            this.transform = transform;
            this.direction = direction;
            this.speed = speed;
            this.distance = distance;
        }

        public override IEnumerator DoMove()
        {
            Vector3 startPos = transform.position;
            Vector3 endPos = transform.position + direction.normalized * distance;

            var moveDist = 0f;

            while (moveDist < distance)
            {
                transform.position += direction.normalized * Time.deltaTime * speed;
                moveDist = Vector3.Distance(startPos, transform.position);
                yield return new WaitForEndOfFrame();
            }

            transform.position = endPos;
        }
    }
}