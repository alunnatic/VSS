﻿using System.Collections;

namespace MoveTypes
{
    /// <summary>
    /// This class serves as a base type that all the actual moves inherit from.
    /// This lets you have a list of type List<MoveTypeBase> and put all the child classes in it, like Move, Turn, etc,
    /// as well as having common properties, like DoMove()
    /// abstract means this class cannot be created directly, only classes that inherit from it can,
    /// and any types that inherit from it have to implement this type's abstract members, like DoMove
    /// this lets us have a Move type and a Turn type, etc, all of which have a DoMove() function,
    /// but what they do in that function is unique to each class
    /// </summary>
    public abstract class MoveTypeBase
    {
        public abstract IEnumerator DoMove();
    }
}