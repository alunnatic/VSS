﻿using System;
using System.Collections;
using UnityEngine;

namespace MoveTypes
{
    /// <summary>
    /// A turn action that rotations the specified amount at a speed 
    /// </summary>
    public class Turn : MoveTypeBase
    {
        Transform transform;
        Quaternion rotation;
        float speed;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rotation">Rotation to apply to the transform</param>
        /// <param name="speed">Rotation speed in radians per second</param>
        public Turn(Transform transform, Quaternion rotation, float speed)
        {
            this.transform = transform;
            this.rotation = rotation;
            this.speed = speed;
        }

        public override IEnumerator DoMove()
        {
            var startAngle = transform.rotation;
            var endAngle = transform.rotation * rotation;

            var t = 0f;
            while (t < 1)
            {
                t += Time.deltaTime * speed;
                transform.localRotation = Quaternion.Slerp(startAngle, endAngle, t);
                yield return new WaitForEndOfFrame();
                //The next line adds motion with the rotation
                transform.position += transform.forward * Time.deltaTime * speed;
            }

            transform.localRotation = endAngle;

            yield break;
        }
    }
}