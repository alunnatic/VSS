﻿using UnityEngine;
using System.Collections;

public class Fire : MonoBehaviour {
  
    public Transform bulletPrefab;
    public float fireRate;
    public float power;


    private float myTime = 0.0F;
    private float nextFire = 0.5F;
    public float fireDelta = 2;

    // Use this for initialization
    //sets the fire rate
    void Start () {
        fireDelta = GetComponent<Stats>().fireRate;
    }
	
	// Update is called once per frame
    //calls the shoot method when the fire button is pressed and the fire rate time has elapsed
	void Update () {
        myTime = myTime + Time.deltaTime;

        if (Input.GetButton("Fire1") && myTime > nextFire)
        {
            nextFire = myTime + fireDelta;
            Shoot();

            nextFire = nextFire - myTime;
            myTime = 0.0F;
        }
    }

    //Makes a new bullet, orients the direction and position, sets the velocity
    //takes the gameobject from the collision and uses it to set the target
    void Shoot()
    {
        var bulletTrans = Instantiate(bulletPrefab);
        bulletTrans.position = transform.position + transform.forward;
        bulletTrans.forward = transform.forward;

        var bullet = bulletTrans.GetComponent<PlayerBullet>();
        bullet.speed = GetComponent<Stats>().bulletSpeed; ;
        bullet.damage = GetComponent<Stats>().bulletPower;
    }
}
