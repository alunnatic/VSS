﻿using UnityEngine;
using System.Collections;

public class PlayerBullet : MonoBehaviour {

    public string bulletTag;
    public float damage;
    public float speed;

    //When the bullet is made, this starts the timer that destroys the bullet in case it misses
    void Start() {
        StartCoroutine(BulletTimeOut());
    }

    // Update is called once per frame, looks at the enemy and moves the bullet toward it
    void Update() {       
        transform.position +=  transform.forward * Time.deltaTime * speed;
    }

    //If the bullet hits an enemy it takes damage (the damage is currently equal to bulletPower located in Stats)
    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Enemy"){
            other.GetComponent<EnemyStats>().health -= damage;
            Destroy(gameObject);
        }              
    }

    //Destroy's the bullet after 2 seconds
    IEnumerator BulletTimeOut(){
        yield return new WaitForSeconds(1.5f);
        Destroy(gameObject);
    }
}
