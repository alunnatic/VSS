﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartLevel : MonoBehaviour {

    public Transform enemy1;

    private void Start()
    {
        
    }
    public void CallThisToStart()
    {
        StartCoroutine(BeginLevel());
    }

    // loops through the waves
    IEnumerator BeginLevel()
    {
        StartCoroutine(SendWave1());
        yield return new WaitForSeconds(5);
        StartCoroutine(SendWave2());
    }

    IEnumerator SendWave1()
    {
        for (int i = 0; i < 6; i++)
        {
            yield return new WaitForSeconds(.3f);    //time between enemies
            Wave1(i);
        }
    }

    void Wave1(int i)
    {
        Vector3[] position = {
            new Vector3(20, 1, -5),
            new Vector3(20, 1, -3),
            new Vector3(20, 1, -1),
            new Vector3(20, 1, 1),
            new Vector3(20, 1, 3),
            new Vector3(20, 1, 5), }; 
        MakeEnemy(enemy1, position[i], 10, 1, 2.5f, 4, 2, 1);
        
    }

    IEnumerator SendWave2()
    {
        for (int i = 0; i < 6; i++)
        {
            yield return new WaitForSeconds(.3f);    //time between enemies
            Wave2(i);
        }
    }

    void Wave2(int i)
    {
        Vector3[] position = {
            new Vector3(20, 1, -7),
            new Vector3(20, 1, -3),
            new Vector3(20, 1, -1),
            new Vector3(20, 1, 1),
            new Vector3(20, 1, 3),
            new Vector3(20, 1, 5), };
        MakeEnemy(enemy1, position[i], 10, 1, 2.5f, 4, 2, 1);
    }

    void MakeEnemy(Transform enemy, Vector3 position, float hlth, float arm, float espeed, float fR, float bS, float bP)
    {
        var enemyMade = Instantiate(enemy);
        enemyMade.position = position;

        enemyMade.GetComponent<EnemyStats>().setAll(hlth, arm, espeed, fR, bS, bP);
        enemyMade.GetComponent<MoveTest>().JustDoIt();

    }
}
